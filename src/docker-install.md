# Installation

Seafile is deployed using Docker Compose

### Deployment

Seafile is deplyed using the following Docker Compose file as a fresh installation:


```yaml
services:
  db:
    restart: "unless-stopped"
    image: mariadb:10.11
    container_name: seafile-mysql
    environment:
      - MYSQL_ROOT_PASSWORD=********  # Requested, set the root's password of MySQL service.
      - MYSQL_LOG_CONSOLE=true
      - MARIADB_AUTO_UPGRADE=1
    volumes:
      - /opt/seafile-mysql/db:/var/lib/mysql  # Requested, specifies the path to MySQL data persistent store.
    networks:
      - seafile-net

  memcached:
    restart: "unless-stopped"
    image: memcached:1.6.18
    container_name: seafile-memcached
    entrypoint: memcached -m 256
    networks:
      - seafile-net

  seafile:
    restart: "unless-stopped"
    image: seafileltd/seafile-mc:latest
    container_name: seafile
    ports:
      - "80:80"
      - "443:443"  # If https is enabled, cancel the comment.
    volumes:
      - /opt/seafile-data:/shared   # Requested, specifies the path to Seafile data persistent store.
    environment:
      - DB_HOST=db
      - DB_ROOT_PASSWD=********  # Requested, the value should be root's password of MySQL service.
      - TIME_ZONE=Europe/Madrid  # Optional, default is UTC. Should be uncomment and set to your local time zone.
      - SEAFILE_ADMIN_EMAIL=***@***.*** # Specifies Seafile admin user, default is 'me@example.com'.
      - SEAFILE_ADMIN_PASSWORD=********     # Specifies Seafile admin password, default is 'asecret'.
      - SEAFILE_SERVER_LETSENCRYPT=true   # Whether to use https or not.
      - SEAFILE_SERVER_HOSTNAME=********.*** # Specifies your host name if https is enabled.
    depends_on:
      - db
      - memcached
    networks:
      - seafile-net

networks:
  seafile-net:

```

### Docker commands

| Description | Docker command |
| --- | --- |
| Start Seafile | `docker compose up -d` |
| Stop Seafile | `docker compose down` |
| Display logs | `docker compose logs` |
| Display running containers | `docker compose ps` |

### Database import

MySQL Database is imported from the backup of the previous Seafile.

The three Databases to import are `ccnet_db`, `seafile_db` and `seahub_db`.

```sh
docker exec -i seafile-mysql /usr/bin/mysql -uroot -pdb_dev ccnet_db < ccnet_db.2024-04-01-16-06-14
docker exec -i seafile-mysql /usr/bin/mysql -uroot -pdb_dev seafile_db < seafile_db.2024-04-01-16-06-14
docker exec -i seafile-mysql /usr/bin/mysql -uroot -pdb_dev seahub_db < seahub_db.2024-04-01-16-06-14
```

### Data import

Directories moved from backup are `seafile-data` and `seahub-data`.

New location of Directories:
```sh
/opt/seafile-data/seafile/seafile-data
/opt/seafile-data/seafile/seahub-data
```

Old location:
```sh
/opt/seafile/seafile-data
/opt/seafile/seahub-data
```