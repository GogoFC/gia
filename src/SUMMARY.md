# Summary

[GIA](./README.md)

---
- [Seafile Docker](./sefile-docker.md)
    - [Installation](./docker-install.md)
    - [Backup](./docker-backup.md)
    - [Restore Procedure](./docker-restore.md)
    - [Info](./docker-info.md)
- [Seafile Version 8](./version8.md)
    - [Installation](./install.md)
    - [Crontab](./cron.md)
    - [Backup](./backup.md)
    - [Restore Procedure](./restore.md)
    - [Transfer Speed](./transferspeed.md)
    - [Scripts](./scripts.md)