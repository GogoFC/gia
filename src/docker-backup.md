# Backup

### Cronjob

Cron runs the `giadrive-backup-script.sh` daily at 1AM.

```sh
@daily bash /root/scripts/giadrive-backup-script.sh #Backup Script for Gia Drive
```

Cron runs as `root` so the files created by the Script are in `/root/` directory.

`/root/scripts/` contains the backup script.

`/root/seafile-docker-compose/` contains the Docker Compose file.

Database and log files are created by the Script and left in the `/root` directory. The most recent 12 versions are kept, backup files older than 12 days are deleted.

Database files in the screenshot are in red color with `.gz` extension. Logs have `.log` extension.

![ls](images/root-ls.png)

### Script

Backup script adopted for Docker

```bash
#!/bin/bash

# (1) set up all the mysqldump variables
FILE0=seafile_db.`date +"%Y-%m-%d-%H-%M-%S"`
FILE1=seahub_db.`date +"%Y-%m-%d-%H-%M-%S"`
FILE2=ccnet_db.`date +"%Y-%m-%d-%H-%M-%S"`
DBSERVER=localhost
DATABASE0=seafile_db
DATABASE1=seahub_db
DATABASE2=ccnet_db
USER=root
PASSWORD=db_dev

unalias rm     2> /dev/null


# use this command for a database server on a separate host:
#mysqldump --opt --protocol=TCP --user=${USER} --host=${DBSERVER} ${DATABASE} > ${FILE}

#docker exec -it seafile-mysql mysqldump  -u${USER} -p${PASSWORD} --opt ${DATABASE0} > ${FILE0}
#docker exec -it seafile-mysql mysqldump  -u${USER} -p${PASSWORD} --opt ${DATABASE1} > ${FILE1}
#docker exec -it seafile-mysql mysqldump  -u${USER} -p${PASSWORD} --opt ${DATABASE2} > ${FILE2}


docker exec -t seafile-mysql mysqldump  -u${USER} -p${PASSWORD} --opt ${DATABASE0} > ${FILE0}
docker exec -t seafile-mysql mysqldump  -u${USER} -p${PASSWORD} --opt ${DATABASE1} > ${FILE1}
docker exec -t seafile-mysql mysqldump  -u${USER} -p${PASSWORD} --opt ${DATABASE2} > ${FILE2}

# (4) gzip the mysql database dump file
gzip $FILE0
gzip $FILE1
gzip $FILE2

# rsync mysqldump
rsync -avHP --delete $FILE0.gz $FILE1.gz $FILE2.gz gia-drive-backup:/root/giadrive-backup/db-backup/ --log-file=seafile-backup-db.log

# Finds files ending in .gz older than 12 days and deletes them
find /root -mtime +12 -type f -name '*.gz' -delete

sleep 1

# rsync seafile data folder
rsync -avHP --delete /opt/seafile-data gia-drive-backup:/root/giadrive-backup/data-backup/ --log-file=seafile-backup-data.log

# rsync logs
rsync -avHP --delete *.log gia-drive-backup:/root/giadrive-backup/logs/
```

- The Script runs `mysqldump` on the `seafile-mysql` container. It dumps the `seafile_db`, `seahub_db` and `ccnet_db` databases and then it transfers the files to `gia-drive-backup` server.

- Using `rsync` it syncs the Data directory `/opt/seafile-data` to `/root/giadrive-backup/` location on the backup server. 

- Then it syncs the log files.


### SSH Config file


`/root/.ssh/config`

```sh
Host gia-drive-backup
  Hostname {IP ADDRESS}
  User root

```