# Auto install Seafile Server

For Seafile 8.0.x or newer versions on Ubuntu 18.04/20.04 (64bit) server

```sh
cd /root
wget --no-check-certificate https://raw.githubusercontent.com/haiwen/seafile-server-installer/master/seafile-8.0_ubuntu
bash seafile-8.0_ubuntu 8.0.0
```

- [Github haiwen seafile-server-installer](https://github.com/haiwen/seafile-server-installer#installation)
