# Cron

```sh
# Renew SSL automatically
15 2 1 * * certbot renew --post-hook 'systemctl restart nginx'

# Backup script daily at midnight

0 23 * * * /root/.backupscript/seafile-data-backup.sh
```
