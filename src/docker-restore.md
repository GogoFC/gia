# Restore Procedure

Restore procedure is similar to install procedure.

- Deploy fresh Seafile with Docker Compose
- Stop `seafile` and `seahub` inside `seafile` Docker Container.
- Import Databases
- Move Data
- Start `seafile` and seahub inside `seafile` Docker Container.

### Deploy Seafile

See [Installation](./docker-install.md) section.

### Stop seafile and seahub

Run the following commands to stop `seafile` and `seahub`
```sh
docker exec -it seafile /opt/seafile/seafile-server-latest/./seafile.sh stop
docker exec -it seafile /opt/seafile/seafile-server-latest/./seahub.sh stop
```

Or enter the Docker container and then run the scripts

Enter `seafile` container
```sh
docker exec -it seafile bash
```
Navigate to server folder
```sh
cd /opt/seafile/seafile-server-latest
```

Execute scripts to stop `seafile` and `seahub`
```sh
./seafile.sh stop
./seahub.sh stop
```

### Import Databases

Import the three Databases: `ccnet_db`, `seafile_db` and `seahub_db`.

```sh
docker exec -i seafile-mysql /usr/bin/mysql -uroot -pdb_dev ccnet_db < ccnet_db.2024-04-01-16-06-14
docker exec -i seafile-mysql /usr/bin/mysql -uroot -pdb_dev seafile_db < seafile_db.2024-04-01-16-06-14
docker exec -i seafile-mysql /usr/bin/mysql -uroot -pdb_dev seahub_db < seahub_db.2024-04-01-16-06-14
```

### Move Data

Delete the directories from the fresh installation:

`/opt/seafile-data/seafile/seafile-data`

`/opt/seafile-data/seafile/seahub-data`

Replace the `seafile-data` and `seahub-data` directories with the backed up ones.

### Start seafile and seahub

Start `seafile` and `seahub` back up again.

Run the following commands to start `seafile` and `seahub`
```sh
docker exec -it seafile /opt/seafile/seafile-server-latest/./seafile.sh start
docker exec -it seafile /opt/seafile/seafile-server-latest/./seahub.sh start
```