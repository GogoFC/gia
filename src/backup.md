# Backup Directories

Cron runs scripts in user's home directory.  

Logs and database backup files are saved to `/root` directory of main server and backed up in `/root/giadrive-backup` directory of the backup server.  
Copy of data is backed up only on backup server.


# Main Server

Mysqldump databases are kept for 12 days, those older than 12 days are deleted. 
They are very small in size. 

Contents of `/root` directory.  
`seafile_db` `seahub_db` `ccnet_db`

![rootmain](images/rootmain.png)

Space used by databases in Kilobytes.

![dbspaceused](images/dbspaceused.png)

Backup Script is located at `/root/.backupscript/seafile-data-backup.sh`

![script](images/scriptfolder.png)


# Backup Server

Backup directory `/root/giadrive-backup`

![backupfolder](images/backupfolder.png)

Databases directory `/root/giadrive-backup/db`

![dbfolder](images/dbfolder.png)

Logs directory `/root/giadrive-backup/logs`

![logs](images/logsfolder.png)

Log Contents

![readlog](images/readlog.png)

Seafile directory `/root/giadrive-backup/data/seafile`

![seafile](images/seafilefolder.png)

Space used

![usedspace](images/spaceused.png)

