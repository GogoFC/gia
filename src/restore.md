# Restore from backup

### 0. Install Seafile

On the new Server [install](./install.md) a fresh Seafile.

### 1. Copy data from backup server.

Copy entire `giadrive-backup` folder from backup server to new server.

```SH
rsync -avHP --delete giadrive-backup root@new.server.dev:/root
```
Login to new server and replace `seaflie-data` and `seahub-data` folders with the ones we backed up. Do not replace anything else in the `/opt/seafile/` directory besides those two directories.

```sh
mv -rf /root/giadrive-backup/data/seafile/seafile-data /opt/seafile/seafile-data
```

```sh
mv -rf /root/giadrive-backup/data/seafile/seahub-data /opt/seafile/seahub-data
```

Restore proper permissions

```sh
chown seafile:seafile /opt/seafile/seafile-data/ /opt/seafile/seahub-data/
```

### 2. Restore the database.

Extract the `.gz`archive fr each database.

```sh
gzip -d ccnet-db.sql.2013-10-19-16-00-05.gz
```

Import the databases. 

```sh
mysql -u root ccnet_db < ccnet-db.sql.2013-10-19-16-00-05
mysql -u root seafile_db < seafile-db.sql.2013-10-19-16-00-20
mysql -u root seahub_db < seahub-db.sql.2013-10-19-16-01-05
```

### 3. Run Seafile Filesystem Check

Run file system check as `seafile` user.

Scripts are located in `cd /opt/seafile/seafile-server-latest` directory.

```sh
sudo -u seafile /opt/seafile/seafile-server-latest/seaf-fsck.sh
```

> Since database and data are backed up separately, they may become a little inconsistent with each other. 
> To correct the potential inconsistency, run seaf-fsck tool to check data integrity on the new machine. 
> [See seaf-fsck documentation](https://manual.seafile.com/maintain/seafile_fsck/).


### 4. Restart Seafile

Restart Seafile and Seahub as `seafile` user.

```sh
sudo -u seafile /opt/seafile/seafile-server-latest/seafile.sh restart
```
```sh
sudo -u seafile /opt/seafile/seafile-server-latest/seahub.sh restart
```

# Seafile docs

- [Restore from backup](https://manual.seafile.com/maintain/backup_recovery/#restore-from-backup)

- [Seafile FSCK](https://manual.seafile.com/maintain/seafile_fsck/)

- [Seafile FSCK](https://www.mianshigee.com/tutorial/Seafile-en/maintain-seafile_fsck.md)

- [Backup using Rsync](https://seafile.readthedocs.io/en/latest/backup/backup-rsync/)

### Seafile FSCK

The seaf-fsck tool accepts the following arguments:

> 1. cd seafile-server-latest
> 2. ./seaf-fsck.sh [--repair|-r] [--export|-E export_path] [repo_id_1 [repo_id_2 ...]]

### Repairing Corruption

> 1. cd seafile-server-latest
> 2. ./seaf-fsck.sh

> 1. cd seafile-server-latest
> 2. ./seaf-fsck.sh --repair

Most of time you run the read-only integrity check first, to find out which libraries are corrupted. And then you repair specific libraries with the following command:

> 1. cd seafile-server-latest
> 2. ./seaf-fsck.sh --repair [library-id1] [library-id2] ...

