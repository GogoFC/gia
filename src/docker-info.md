# Info

Other info regarding the Servers

### UFW

UFW is enabled

To check the status run:
```sh
ufw status
```

### Fail2ban

Fail2ban is enabled

To check the fail2ban status run:
```sh
fail2ban-client status sshd
```

