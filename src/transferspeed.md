# Data Transfer Speed

## Recovery data transfer speed considerations. 

In the event where a data recovery is needed, it would take 4 hours 25 minutes to complete a transfer of 200 GB of data. 



## Next scheduled backups

Time to complete the next scheduled backup will depend on daily data added. Predicting about 5 minutes each day. 


| Additional Data   | Time to complete  |
|--------|------------|
| 2 GB   |  3 Minutes |
| 5 GB   | 7 Minutes |
| 10 GB  | 14 Minutes |
| 100 GB |  2 Hours   |
| 200 GB | 5 Hours   |
|500 GB  | 11 Hours  |
